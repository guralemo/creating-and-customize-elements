import React from "react";
import ReactDOM from "react-dom/client";
import "./style.css";

const title = React.createElement(
  "h1",
  { style: { color: "#999", fontSize: "19px" } },
  "Solar system planets"
);

let ul = React.createElement(
  "ul",
  { className: "planets-list" },
  React.createElement("li", null, "Mercury"),
  React.createElement("li", null, "Venus"),
  React.createElement("li", null, "Earth"),
  React.createElement("li", null, "Mars"),
  React.createElement("li", null, "Jupiter"),
  React.createElement("li", null, "Saturn"),
  React.createElement("li", null, "Uranus"),
  React.createElement("li", null, "Neptune")
);
function App() {
  return (
    <div>
      {title}
      {ul}
    </div>
  );
}
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
